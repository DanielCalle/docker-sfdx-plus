FROM openjdk:8
LABEL maintainer "Daniel Calle <danielcallesanchez@gmail.com>"

ENV DEBIAN_FRONTEND=noninteractive
ENV PMD_VERSION="6.24.0"
ENV NODE_VERSION="14.15.0"

ENV SFDX_AUTOUPDATE_DISABLE false
ENV SFDX_USE_GENERIC_UNIX_KEYCHAIN true
ENV SFDX_DOMAIN_RETRY 600
ENV SFDX_LOG_LEVEL DEBUG
ENV NODEJS_URL https://deb.nodesource.com/node_14.x/pool/main/n/nodejs/nodejs_${NODE_VERSION}-deb-1nodesource1_amd64.deb
ENV PMD_URL https://github.com/pmd/pmd/releases/download/pmd_releases/${PMD_VERSION}/pmd-bin-${PMD_VERSION}.zip
ENV SALESFORCE_CLI_URL https://developer.salesforce.com/media/salesforce-cli/sfdx-linux-amd64.tar.xz

# Install needing packages
RUN apt update && apt install -y jq wget zip unzip python3-minimal

# NODE
RUN wget $NODEJS_URL \
    && dpkg -i nodejs_14.15.0-deb-1nodesource1_amd64.deb \
    && rm nodejs_14.15.0-deb-1nodesource1_amd64.deb
RUN npm --global config set user root
ENV PATH="/usr/lib/node_modules:${PATH}"

# PMD
RUN mkdir -p /opt \
    && cd /opt \
    && wget -nc -O pmd.zip $PMD_URL \
    && unzip pmd.zip \
    && rm -f pmd.zip \
    && mv /opt/pmd-bin-${PMD_VERSION} /opt/pmd
ENV PATH="/opt/pmd:${PATH}"

# SFDX
RUN npm install sfdx-cli --global

# ESLint
RUN npm install eslint prettier --global

# SFDX Plugins
RUN mkdir -p $HOME/.config/sfdx
RUN echo "[\"@dx-cli-toolbox/sfdx-toolbox-package-utils\", \"sfdx-git-delta\"]" >> $HOME/.config/sfdx/unsignedPluginAllowList.json
RUN sfdx plugins:install sfdx-git-delta
RUN sfdx plugins:install @dx-cli-toolbox/sfdx-toolbox-package-utils

# Clean cache
RUN npm cache verify