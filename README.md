# Docker image for CI using SFDX with other tools
Dockerfile for Salesforce DX with PMD and ESLint.

The main use of this Dockerfile is for continuous integration environments. We could use SFDX in addition to PMD and ESLint in order to improve the quality and stability of our projects for the Salesforce platform.

You can use this docker from [dacalle/sfdxplus](https://hub.docker.com/r/dacalle/sfdxplus)